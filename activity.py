# Validation has been added to each part of the activity (method is explained in first part)
# ---------------------------------------------------------------------------------------------- #
# Accept a year input from the user and determine if it is a leap year or not.

year = input("Please enter a year:\n")

# Validation - checks if the user input is an integer
while not year.isdigit():
    year = input("Sorry but you need to enter an INTEGER greater than zero (0).\nPlease enter a year:\n")

# Validation - now that the user has entered an integer, it must be typecast
year = int(year)

# Validation - checks if the user input is greater than zero (0)
while year < 1:
    year = input("Sorry but you need to enter an INTEGER greater than zero (0).\nPlease enter another year:\n")

if year % 4 == 0:
    print(f"{year} is a leap year!")
elif year % 400 == 0:
    print(f"{year} is a leap year!")
else:
    print(f"{year} is NOT a leap year!")

# ---------------------------------------------------------------------------------------------- #

#  Accept two numbers (row and col) from the user and create a grid of asterisks using the two numbers (row and col).

# Row input
rows = input("Please enter number of rows:\n")

while not rows.isdigit():
    rows = input("Sorry but you need to enter an INTEGER greater than zero (0).\nPlease enter number of rows:\n")

rows = int(rows)

while rows < 1:
    rows = input("Sorry but you need to enter an INTEGER greater than zero (0).\nPlease enter number of rows:\n")

# Column input
columns = input("Please enter number of columns:\n")

while not columns.isdigit():
    columns = input("Sorry but you need to enter an INTEGER greater than zero (0).\nPlease enter number of columns:\n")

columns = int(columns)

while columns < 1:
    columns = input("Sorry but you need to enter an INTEGER greater than zero (0).\nPlease enter number of columns:\n")

# Prints asterisks
ctrK = 0
while ctrK < rows:
    ctrJ = 0
    while ctrJ < columns:
        print("*", end="")
        ctrJ += 1
    print()
    ctrK += 1

